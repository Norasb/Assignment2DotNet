USE SuperheroesDb;

CREATE TABLE Superhero (
	SuperheroId int IDENTITY(1,1) NOT NULL,
	Name varchar(20),
	Alias varchar(20),
	Origin varchar(20),
	PRIMARY KEY (SuperheroId)
);

CREATE TABLE Assistant (
	AssistantId int IDENTITY(1,1) NOT NULL,
	Name varchar(20),
	PRIMARY KEY (AssistantId)
);

CREATE TABLE Power (
	PowerId int IDENTITY(1,1) NOT NULL,
	Name varchar(20),
	Description varchar(100),
	PRIMARY KEY (PowerId)
);