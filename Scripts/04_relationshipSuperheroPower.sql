Use SuperheroesDb;

CREATE TABLE SuperheroPowerLink (
	SuperheroId int,
	PowerId int,
	PRIMARY KEY(SuperheroId, PowerId)
	);

ALTER TABLE SuperheroPowerLink 
	ADD CONSTRAINT fk_Superhero2 FOREIGN KEY (SuperheroId) REFERENCES Superhero(SuperheroId),
	    CONSTRAINT fk_Power FOREIGN KEY (PowerId) REFERENCES Power(PowerId);
	