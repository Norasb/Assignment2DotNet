Use SuperheroesDb;

INSERT INTO Power (Name, Description)
VALUES ('Lightning Speed', 'The ability to run at the speed of lightning');

INSERT INTO Power (Name, Description)
VALUES ('Pastasweat', 'The ability to sweat pasta water');

INSERT INTO Power (Name, Description)
VALUES ('Smarties Smart', 'The ability to get really smart when eating smarties');

INSERT INTO Power (Name, Description)
VALUES ('Sverri', 'The ability to be as Sverri�s as Sverre');
			
			

INSERT INTO SuperheroPowerLink (SuperheroId, PowerId)
VALUES (3, 1);

INSERT INTO SuperheroPowerLink (SuperheroId, PowerId)
VALUES (2, 4);

INSERT INTO SuperheroPowerLink (SuperheroId, PowerId)
VALUES (1, 2);

INSERT INTO SuperheroPowerLink (SuperheroId, PowerId)
VALUES (1, 3);

