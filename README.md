# Assignment2DotNet



#### About

Basic database manipulation and setup is what this repo contains. The languages used are C# for for manipulation and Transact-SQL for queries. With this repo you have the ability to manipulate a database called Chinook. the methods are centered around the Customer table and gie you the ability to add to and get information surrounding this table.

--- 

#### Installing and running

To use this repo you need to install three things:

- Visual studio

- SQL server

- SQL server management studio

To run you first need to create the local Chinook database by running the Chinook_SqlServer_AutoIncrementPKs.sql script. Then you can uncomment the functioncall you want to test.

---

#### Authors

Nora Sophie Bache - [Nora Sophie Backe · GitLab](https://gitlab.com/Norasb)

Tord Vetle Gjertsen - [Tord Vetle Gjertsen · GitLab](https://gitlab.com/Tartarpaste)






