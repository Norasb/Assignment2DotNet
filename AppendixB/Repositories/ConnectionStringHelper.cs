﻿using Microsoft.Data.SqlClient;

namespace AppendixB.Repositories {
    internal class ConnectionStringHelper {

        /// <summary>
        /// Establishes connection with the database
        /// </summary>
        /// <returns>Connectionstring</returns>
        public static string GetConnectionString() {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = "LAPTOP-U1LOIIA6\\SQLEXPRESS"; //Insert your own DataSource here
            builder.InitialCatalog = "Chinook";
            builder.IntegratedSecurity = true;
            builder.TrustServerCertificate= true;
            return builder.ConnectionString;
        }
    }
}
