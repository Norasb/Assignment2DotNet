﻿using AppendixB.Models;

namespace AppendixB.Repositories
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets one customer based on id
        /// </summary>
        /// <param id="id"></param>
        /// <returns>One customer with Id, Firstname, Lastname, Country, Postalcode, Phone and Email</returns>
        public Customer GetCustomer(int id);
        /// <summary>
        /// Gets one customer based on name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>One customer with Id, Firstname, Lastname, Country, Postalcode, Phone and Email</returns>
        public Customer GetCustomerByName(string name);
        /// <summary>
        /// Gets all customers
        /// </summary>
        /// <returns>A list with Id, Firstname, Lastname, Country, Postalcode, Phone and Email</returns>
        public List<Customer> GetAllCustomers();
        /// <summary>
        /// Gets a list of customers based on an prefixed offset and limited amount
        /// </summary>
        /// <returns>A list of the limited customers with Id, Firstname, Lastname, Country, Postalcode, Phone and Email</returns>
        public List<Customer> GetLimitedCustomers();
        /// <summary>
        /// Adds a new customer to the database with fields: Id, Firstname, Lastname, Country, Postalcode, Phone and Email
        /// </summary>
        /// <param Customer="{customer}"></param>
        /// <returns>Either true or false</returns>
        public bool addNewCustomer(Customer customer);
        /// <summary>
        /// To update a customer in the database with fields: Id, Firstname, Lastname, Country, Postalcode, Phone and Email
        /// </summary>
        /// <param customer="{customer}"></param>
        /// <returns>Either true or false</returns>
        public bool updateCustomer(Customer customer, int id);
        /// <summary>
        /// Counts all customers each country
        /// </summary>
        /// <returns>A list of the countries and the count of customers in each country</returns>
        public List<CustomerCountry> CountryCount();
        /// <summary>
        /// To find what each customer spends
        /// </summary>
        /// <returns>A list with each customer and the total sum of what they spends</returns>
        public List<CustomerSpender> customerSpenders();
        /// <summary>
        /// To find the most bought genre of a specific customer found with id
        /// </summary>
        /// <param id="id"></param>
        /// <returns>One customer with the genre or more genres if the count is the same</returns>
        public List<CustomerGenre> CustomerGenre(int id);

    }
}
