﻿using AppendixB.Models;
using Microsoft.Data.SqlClient;

namespace AppendixB.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader()) 
                        {
                            while (reader.Read())
                            {
                                Customer temporaryCustomer = new Customer();
                                temporaryCustomer.CustomerId = reader.GetInt32(0);
                                temporaryCustomer.FirstName = reader.GetString(1);
                                temporaryCustomer.LastName = reader.GetString(2);
                                temporaryCustomer.Country= reader.GetString(3);
                                temporaryCustomer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temporaryCustomer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temporaryCustomer.Email = reader.GetString(6);
                                customerList.Add(temporaryCustomer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<Customer> GetLimitedCustomers() 
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email \r\nFROM Customer \r\nORDER BY CustomerId \r\n\tOFFSET 10 ROWS \r\n\tFETCH NEXT 10 ROWS ONLY;";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temporaryCustomer = new Customer();
                                temporaryCustomer.CustomerId = reader.GetInt32(0);
                                temporaryCustomer.FirstName = reader.GetString(1);
                                temporaryCustomer.LastName = reader.GetString(2);
                                temporaryCustomer.Country = reader.GetString(3);
                                temporaryCustomer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                temporaryCustomer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                temporaryCustomer.Email = reader.GetString(6);
                                customerList.Add(temporaryCustomer);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return customerList;
        }

        public Customer GetCustomer(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";
           
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("CustomerId", id);
                        using (SqlDataReader reader = command.ExecuteReader()) 
                        {
                            while (reader.Read()) 
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.IsDBNull(4) ? null : reader.GetString(4);
                                customer.Phone = reader.IsDBNull(5) ? null : reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            } 
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customer;
        }

        public Customer GetCustomerByName(string name)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName = @FirstName";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("FirstName", name);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);
                                customer.Country = reader.GetString(3);
                                customer.PostalCode = reader.GetString(4);
                                customer.Phone = reader.GetString(5);
                                customer.Email = reader.GetString(6);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customer;
        }

        public bool addNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName,LastName,Country,PostalCode,Phone,Email) " +
                "VALUES (@FirstName,@LastName,@Country,@PostalCode,@Phone,@Email)";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    { 
                        command.Parameters.AddWithValue("@Firstname", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }

                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

        public bool updateCustomer(Customer customer, int id)
        {
            bool success = false;
            string sql = "UPDATE Customer " + 
                            "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email " + 
                            "WHERE CustomerId = CustomerId";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("CustomerId", id);
                        command.Parameters.AddWithValue("@Firstname", customer.FirstName);
                        command.Parameters.AddWithValue("@LastName", customer.LastName);
                        command.Parameters.AddWithValue("@Country", customer.Country);
                        command.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        command.Parameters.AddWithValue("@Phone", customer.Phone);
                        command.Parameters.AddWithValue("@Email", customer.Email);
                        success = command.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return success;
        }

        public List<CustomerCountry> CountryCount()
        {
            List<CustomerCountry> customerCountry = new List<CustomerCountry>();
            string sql = "SELECT Country, COUNT(*) AS Count\r\nFROM Customer\r\nGROUP BY Country\r\nORDER BY Count DESC;";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry country = new CustomerCountry();
                                country.Country = reader.GetString(0);
                                country.Count = reader.GetInt32(1);
                                customerCountry.Add(country);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return customerCountry;
        }

        public List<CustomerSpender> customerSpenders()
        {
            List<CustomerSpender> customerSpender = new List<CustomerSpender>();
            string sql = "SELECT Customer.FirstName, SUM(Invoice.Total) as CustomerTotal\r\nFROM Invoice \r\nINNER JOIN Customer ON Invoice.CustomerId=Customer.CustomerId\r\nGROUP BY Invoice.CustomerId, Customer.FirstName\r\nORDER BY CustomerTotal DESC;";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerSpender spender = new CustomerSpender();
                                spender.Firstname = reader.GetString(0);
                                spender.CustomerTotal = reader.GetDecimal(1);
                                customerSpender.Add(spender);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return customerSpender;
        }

        public List<CustomerGenre> CustomerGenre(int id)
        {
            List<CustomerGenre> customerGenre = new List<CustomerGenre>();
            string sql = "SELECT Customer.FirstName, Genre.Name AS MostRecurringGenre\r\nFROM Customer\r\nINNER JOIN (\r\n    SELECT CustomerId, GenreId,\r\n           DENSE_RANK() OVER (PARTITION BY CustomerId ORDER BY COUNT(*) DESC) AS rank\r\n    FROM Invoice\r\n    INNER JOIN InvoiceLine ON Invoice.InvoiceId=InvoiceLine.InvoiceId\r\n    INNER JOIN Track ON InvoiceLine.TrackId=Track.TrackId\r\n    GROUP BY CustomerId, GenreId\r\n) AS customer_genre_rank\r\nON Customer.CustomerId = customer_genre_rank.CustomerId\r\nINNER JOIN Genre ON customer_genre_rank.GenreId = Genre.GenreId\r\nWHERE customer_genre_rank.rank = 1 AND Customer.CustomerId = @CustomerId\r\nORDER BY Customer.FirstName;";
            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("CustomerId", id);
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre genre = new CustomerGenre();
                                genre.FirstName = reader.GetString(0);
                                genre.Name = reader.GetString(1);
                                customerGenre.Add(genre);
                            }
                        }
                    }
                }

            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex);
            }
            return customerGenre;
        }
    }
}
