﻿using AppendixB.Models;
using AppendixB.Repositories;

namespace AppendixB {
    class Program {
        static void Main(string[] args) {
            ICustomerRepository repository = new CustomerRepository();

            //To test the functions uncomment the functioncall you want to test
            //SelectAll(repository);
            //SelectWithId(repository);
            //SelectWithName(repository);
            //SelectLimited(repository);
            //AddCustomer(repository);
            //UpdateCustomer(repository);
            //SelectCountryCount(repository);
            //SelectSpenders(repository);
            //SelectGenre(repository);
        }

        //Methods to test the database manipulation queries
        static void SelectGenre(ICustomerRepository repository) {
            PrintGenres(repository.CustomerGenre(12));
        }

        static void SelectSpenders(ICustomerRepository repository) {
            PrintTotals(repository.customerSpenders());
        }

        static void SelectCountryCount(ICustomerRepository repository) {
            PrintCountries(repository.CountryCount());
        }

        static void SelectLimited(ICustomerRepository repository) {
            PrintCustomers(repository.GetLimitedCustomers());
        }

        static void SelectAll(ICustomerRepository repository) {
            PrintCustomers(repository.GetAllCustomers());
        }

        static void SelectWithId(ICustomerRepository repository) {
            PrintCustomer(repository.GetCustomer(1));
        }

        static void SelectWithName(ICustomerRepository repository) {
            PrintCustomer(repository.GetCustomerByName("Nicholas"));
        }

        static void AddCustomer(ICustomerRepository repository) {
            Customer customer = new Customer() {
                FirstName = "Nicholas",
                LastName = "Necrobutcher",
                Country = "New Tristram",
                PostalCode = "0221",
                Phone = "+47 53180008",
                Email = "nickmaister@gmail.com",
            };
            if (repository.addNewCustomer(customer)) {
                PrintCustomer(repository.GetCustomer(60));
            } else {
                Console.WriteLine("Didn't work");
            }
        }

        static void UpdateCustomer(ICustomerRepository repository) {
            Customer customer = new Customer() {
                FirstName = "Dean",
                LastName = "Seraphim",
                Country = "Langtbortistan",
                PostalCode = "0001",
                Phone = "+47 01134134",
                Email = "deanmaister@gmail.com",
            };
            if (repository.updateCustomer(customer, 60)) {
                PrintCustomer(repository.GetCustomer(60));
            } else {
                Console.WriteLine("Didn't work");
            }
        }

        //Methods to print the database information
        static void PrintCustomers(IEnumerable<Customer> customers) {
            foreach (Customer customer in customers) {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer) {
            Console.WriteLine($" {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email} ");
        }

        static void PrintCountry(CustomerCountry customerCountry) {
            Console.WriteLine($" {customerCountry.Country}: { customerCountry.Count } ");
        }

        static void PrintCountries(IEnumerable<CustomerCountry> customerCountries) {
            foreach (CustomerCountry customerCountry in customerCountries) {
                PrintCountry(customerCountry);
            }
        }

        static void PrintTotals(IEnumerable<CustomerSpender> customerSpenders) {
            foreach (CustomerSpender customerSpender in customerSpenders) {
                PrintTotal(customerSpender);
            }
        }

        static void PrintTotal(CustomerSpender customerSpender) {
            Console.WriteLine($" {customerSpender.Firstname} {customerSpender.CustomerTotal} ");
        }

        static void PrintGenres(IEnumerable<CustomerGenre> customerGenres) {
            foreach (CustomerGenre customerGenre in customerGenres) {
                PrintGenre(customerGenre);
            }
        }

        static void PrintGenre(CustomerGenre customerGenre) {
            Console.WriteLine($" {customerGenre.FirstName} {customerGenre.Name} ");
        }
    }
}

