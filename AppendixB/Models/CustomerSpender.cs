﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppendixB.Models
{
    public class CustomerSpender
    {

        public string Firstname { get; set; } = null!;
        public decimal CustomerTotal { get; set; }
    }
}
